// GLOBALS
var prevX = 0;
var prevY = 0;
var swipeProgress = 0;

// GESTURE: Clap
function clap(leftHandPositionX, leftHandPositionY, rightHandPositionX, rightHandPositionY) {
      /*Calculate position on screen*/
      var clapRelativePositionX = rightHandPositionX - leftHandPositionX;
      var clapRelativePositionY = rightHandPositionY - leftHandPositionY;
      var window_width = $(window).width();
      var window_height = $(window).height();
      var x = Math.round(rightHandPositionX * window_width);
      var y = Math.round(rightHandPositionY * window_height);
      /*temporary vars*/
      var clapX;
      var clapY;

      // hand.push(new Hand(hand_id, x, y));
      // hand_id = hand_id + 1;

      if (clapRelativePositionX < 0.01 && clapRelativePositionX > -0.01 && clapRelativePositionY < 0.01 && clapRelativePositionY > -0.01) {

          clapX = x;
          clapY = y;

          if (!clapped) {
              // console.log("Clap");
              clapped = true;
              bird.push(new Bird(bird_id, clapX, clapY));

  		          bird_id = bird_id + 1;

              setTimeout(function(){
                  clapped = false;
                  // console.log("clapklaar");
            },500);
          }
      }
}

//GESTURE: Swipe
function swipe(rightHandPositionX, rightHandPositionY) {
    var window_width = $(window).width();
    var window_height = $(window).height();
    var x = Math.round(rightHandPositionX * window_width);
    var y = Math.round(rightHandPositionY * window_height);


    var deltaX = x - prevX;
    var deltaY = y - prevY;
    if (deltaX != 0 && deltaY != 0) {
        var direction = deltaY / Math.abs(deltaX) + 0.01;
        // console.log(direction);
        if (direction < -2) {
            // console.log('^');
            swipeProgress += Math.abs(deltaY);
            if(swipeProgress > 150){
              swipeProgress = 0;
              tree.push(new Tree(tree_id, x));
              tree_id = tree_id + 1;
            }

        }
        else {
            // console.log('v');
            swipeProgress = 0;
        }
    }
    // document.getElementById("testmeter").style.height = swipeProgress+'px';

    prevX = x;
    prevY = y;
}

//GESTURE PINCH LEFT HAND
function pinch(leftHandPositionX, leftHandPositionY, rightHandPositionX, rightHandPositionY, leftHandState, rightHandState, body_index){
  var window_width = $(window).width();
  var window_height = $(window).height();
  var left_x = Math.round(leftHandPositionX * window_width);
  var left_y = Math.round(leftHandPositionY * window_height);
  var right_x = Math.round(rightHandPositionX * window_width);
  var right_y = Math.round(rightHandPositionY * window_height);

    //################### BODY 1 ###################//

    if(body_index == 0){
      //LEFT
      if(leftHandState == 3){

          var closedLX = left_x;
          var closedLY = left_y;

          if(!left_kloost){
                left_kloost = true;
                  star.push(new Star(star_id, closedLX, closedLY));
                  star_id = star_id + 1;
          }
      }
      if(left_kloost && leftHandState == 2){
        left_kloost = false;
      }

      //LEFT
      if(rightHandState == 3){

          var closedRX = right_x;
          var closedRY = right_y;

          if(!right_kloost){
                right_kloost = true;
                  star.push(new Star(star_id, closedRX, closedRY));
                  star_id = star_id + 1;
          }
      }
      if(right_kloost && rightHandState == 2){
        right_kloost = false;
      }

    }

    //################### BODY 2 ###################//

    if(body_index == 1){
      //LEFT
      if(leftHandState == 3){

          var closedLX = left_x;
          var closedLY = left_y;

          if(!left_kloost){
                left_kloost = true;
                  star.push(new Star(star_id, closedLX, closedLY));
                  star_id = star_id + 1;
          }
      }
      if(left_kloost && leftHandState == 2){
        left_kloost = false;
      }

      //LEFT
      if(rightHandState == 3){

          var closedRX = right_x;
          var closedRY = right_y;

          if(!right_kloost){
                right_kloost = true;
                  star.push(new Star(star_id, closedRX, closedRY));
                  star_id = star_id + 1;
          }
      }
      if(right_kloost && rightHandState == 2){
        right_kloost = false;
      }

    }

    //################### BODY 3 ###################//

    if(body_index == 2){
      //LEFT
      if(leftHandState == 3){

          var closedLX = left_x;
          var closedLY = left_y;

          if(!left_kloost){
                left_kloost = true;
                  star.push(new Star(star_id, closedLX, closedLY));
                  star_id = star_id + 1;
          }
      }
      if(left_kloost && leftHandState == 2){
        left_kloost = false;
      }

      //LEFT
      if(rightHandState == 3){

          var closedRX = right_x;
          var closedRY = right_y;

          if(!right_kloost){
                right_kloost = true;
                  star.push(new Star(star_id, closedRX, closedRY));
                  star_id = star_id + 1;
          }
      }
      if(right_kloost && rightHandState == 2){
        right_kloost = false;
      }

    }

    //################### BODY 4 ###################//

    if(body_index == 3){
      //LEFT
      if(leftHandState == 3){

          var closedLX = left_x;
          var closedLY = left_y;

          if(!left_kloost){
                left_kloost = true;
                  star.push(new Star(star_id, closedLX, closedLY));
                  star_id = star_id + 1;
          }
      }
      if(left_kloost && leftHandState == 2){
        left_kloost = false;
      }

      //LEFT
      if(rightHandState == 3){

          var closedRX = right_x;
          var closedRY = right_y;

          if(!right_kloost){
                right_kloost = true;
                  star.push(new Star(star_id, closedRX, closedRY));
                  star_id = star_id + 1;
          }
      }
      if(right_kloost && rightHandState == 2){
        right_kloost = false;
      }

    }

    //################### BODY 5 ###################//

    if(body_index == 4){
      //LEFT
      if(leftHandState == 3){

          var closedLX = left_x;
          var closedLY = left_y;

          if(!left_kloost){
                left_kloost = true;
                  star.push(new Star(star_id, closedLX, closedLY));
                  star_id = star_id + 1;
          }
      }
      if(left_kloost && leftHandState == 2){
        left_kloost = false;
      }

      //LEFT
      if(rightHandState == 3){

          var closedRX = right_x;
          var closedRY = right_y;

          if(!right_kloost){
                right_kloost = true;
                  star.push(new Star(star_id, closedRX, closedRY));
                  star_id = star_id + 1;
          }
      }
      if(right_kloost && rightHandState == 2){
        right_kloost = false;
      }

    }

    //################### BODY 6 ###################//

    if(body_index == 5){
      //LEFT
      if(leftHandState == 3){

          var closedLX = left_x;
          var closedLY = left_y;

          if(!left_kloost){
                left_kloost = true;
                  star.push(new Star(star_id, closedLX, closedLY));
                  star_id = star_id + 1;
          }
      }
      if(left_kloost && leftHandState == 2){
        left_kloost = false;
      }

      //LEFT
      if(rightHandState == 3){

          var closedRX = right_x;
          var closedRY = right_y;

          if(!right_kloost){
                right_kloost = true;
                  star.push(new Star(star_id, closedRX, closedRY));
                  star_id = star_id + 1;
          }
      }
      if(right_kloost && rightHandState == 2){
        right_kloost = false;
      }

    }


}
