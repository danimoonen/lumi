//working socket.io and get bodyframe data
console.log("connection gefiext :D ");
var socket = io.connect('localhost:8000');
socket.on('bodyFrame', interpretData);
// Creating global variables
var clapped = false;
var left_kloost = false;
var right_kloost = false;
var bird = [];
var tree = [];
var star = [];
var bird_id = 1;
var tree_id = 1;
var star_id = 1;

function interpretData(bodyFrame) {

	// Move the birds (and potentially delete birds from array and DOM).
    for(var i = 0; i < bird.length; i++){
            bird[i].move();

        if(bird[i].outside_window == true){
            bird[i].delete();
            bird.splice(i,1);
        }
    }

	// Age the trees (and potentially delete trees from array and DOM).
	for(var i = 0; i < tree.length; i++){
	    tree[i].counter();

	    if(tree[i].dead == true){
	        tree[i].delete();
	        tree.splice(i,1);
	    }
	}
  // Age the stars (and potentially delete stars from array and DOM).
  for(var i = 0; i < star.length; i++){
      star[i].counter();

      if(star[i].dead == true){
	        star[i].delete();
	        star.splice(i,1);
      }
  }

    // Update the info panel.
    bodyTracked(bodyFrame.bodies);
    birdCount(bird.length);
    treeCount(tree.length);

    bodyFrame.bodies.forEach(function(body) {
        if (body.tracked) {
      //  	console.log(body);
        	var window_width = $(window).width();
        	var window_height = $(window).height();

            /*Track hand positions*/
        	var body_index = body.bodyIndex;
            var rightHandPositionX = body.joints[11].depthX;
        	var rightHandPositionY = body.joints[11].depthY;
        	var leftHandPositionX = body.joints[7].depthX;
        	var leftHandPositionY = body.joints[7].depthY;
          var leftHandState = body.leftHandState;
          var rightHandState = body.rightHandState;

        	// Functions voor infoscreen.
            handsTracked(body_index, leftHandPositionX, leftHandPositionY, rightHandPositionX, rightHandPositionY);

      		// for(var i = 0; i < hand.length; i++){
		    //   hand[i].reposition(leftHandPositionX, leftHandPositionY);
		    // }

            // Gesture functions
            clap(leftHandPositionX, leftHandPositionY, rightHandPositionX, rightHandPositionY);
            swipe(rightHandPositionX, rightHandPositionY);
            pinch(leftHandPositionX, leftHandPositionY, rightHandPositionX, rightHandPositionY, leftHandState, rightHandState, body_index);

          var left_hand_position_x = body.joints[7].depthX * window_width;
        	var left_hand_position_y = body.joints[7].depthY * window_height;
        	var right_hand_position_x = body.joints[11].depthX * window_width;
        	var right_hand_position_y = body.joints[11].depthY * window_height;

        	// var body = $('body');
	        if(body.bodyIndex == 0){
	        	if(body.leftHandState == 4){
	        		$('#hand-1').css('background-image', 'url(images/hand-left-lasso.png');
	        	}else if(body.leftHandState == 3){
	        		$('#hand-1').css('background-image', 'url(images/hand-left-closed.png');
	        	}else{
	        		$('#hand-1').css('background-image', 'url(images/hand-left-open.png');
	        	}

	        	if(body.rightHandState == 4){
	        		$('#hand-2').css('background-image', 'url(images/hand-right-lasso.png');
	        	}else if(body.rightHandState == 3){
	        		$('#hand-2').css('background-image', 'url(images/hand-right-closed.png');
	        	}else{
	        		$('#hand-2').css('background-image', 'url(images/hand-right-open.png');
	        	}
	        	// var body = $('body');
	        	// body.append('<div class="hand" id="hand-1" style="top:'+left_hand_position_y+'; left:'+left_hand_position_x+';"></div>');
	        	// body.append('<div class="hand" id="hand-2" style="top:'+right_hand_position_y+'; left:'+right_hand_position_x+';"></div>');
	        	$('#hand-1').css('display', 'block');
		      	$('#hand-2').css('display', 'block');
		      	$('#hand-1').css('top', left_hand_position_y);
		      	$('#hand-1').css('left', left_hand_position_x);
		      	$('#hand-2').css('top', right_hand_position_y);
		      	$('#hand-2').css('left', right_hand_position_x);
	        }
	      	else if(body.bodyIndex == 1){
	      		if(body.leftHandState == 4){
	        		$('#hand-3').css('background-image', 'url(images/hand-left-lasso.png');
	        	}else if(body.leftHandState == 3){
	        		$('#hand-3').css('background-image', 'url(images/hand-left-closed.png');
	        	}else{
	        		$('#hand-3').css('background-image', 'url(images/hand-left-open.png');
	        	}

	        	if(body.rightHandState == 4){
	        		$('#hand-4').css('background-image', 'url(images/hand-right-lasso.png');
	        	}else if(body.rightHandState == 3){
	        		$('#hand-4').css('background-image', 'url(images/hand-right-closed.png');
	        	}else{
	        		$('#hand-4').css('background-image', 'url(images/hand-right-open.png');
	        	}
	      		// var body = $('body');
	      		// body.append('<div class="hand" id="hand-3" style="top:'+left_hand_position_y+'; left:'+left_hand_position_x+';"></div>');
	        // 	body.append('<div class="hand" id="hand-4" style="top:'+right_hand_position_y+'; left:'+right_hand_position_x+';"></div>');
	      		$('#hand-3').css('display', 'block');
		      	$('#hand-4').css('display', 'block');
		      	$('#hand-3').css('top', left_hand_position_y);
		      	$('#hand-3').css('left', left_hand_position_x);
		      	$('#hand-4').css('top', right_hand_position_y);
		      	$('#hand-4').css('left', right_hand_position_x);
	      	}
	      	else if(body.bodyIndex == 2){
	      		if(body.leftHandState == 4){
	        		$('#hand-5').css('background-image', 'url(images/hand-left-lasso.png');
	        	}else if(body.leftHandState == 3){
	        		$('#hand-5').css('background-image', 'url(images/hand-left-closed.png');
	        	}else{
	        		$('#hand-5').css('background-image', 'url(images/hand-left-open.png');
	        	}

	        	if(body.rightHandState == 4){
	        		$('#hand-6').css('background-image', 'url(images/hand-right-lasso.png');
	        	}else if(body.rightHandState == 3){
	        		$('#hand-6').css('background-image', 'url(images/hand-right-closed.png');
	        	}else{
	        		$('#hand-6').css('background-image', 'url(images/hand-right-open.png');
	        	}
	      		// var body = $('body');
	      		// body.append('<div class="hand" id="hand-5" style="top:'+left_hand_position_y+'; left:'+left_hand_position_x+';"></div>');
	        // 	body.append('<div class="hand" id="hand-6" style="top:'+right_hand_position_y+'; left:'+right_hand_position_x+';"></div>');
	      		$('#hand-5').css('display', 'block');
		      	$('#hand-6').css('display', 'block');
	      		$('#hand-5').css('top', left_hand_position_y);
	      		$('#hand-5').css('left', left_hand_position_x);
	      		$('#hand-6').css('top', right_hand_position_y);
	      		$('#hand-6').css('left', right_hand_position_x);
	      	}
	      	else if(body.bodyIndex == 3){
	      		if(body.leftHandState == 4){
	        		$('#hand-7').css('background-image', 'url(images/hand-left-lasso.png');
	        	}else if(body.leftHandState == 3){
	        		$('#hand-7').css('background-image', 'url(images/hand-left-closed.png');
	        	}else{
	        		$('#hand-7').css('background-image', 'url(images/hand-left-open.png');
	        	}

	        	if(body.rightHandState == 4){
	        		$('#hand-8').css('background-image', 'url(images/hand-right-lasso.png');
	        	}else if(body.rightHandState == 3){
	        		$('#hand-8').css('background-image', 'url(images/hand-right-closed.png');
	        	}else{
	        		$('#hand-8').css('background-image', 'url(images/hand-right-open.png');
	        	}
	      		// var body = $('body');
	      		// body.append('<div class="hand" id="hand-7" style="top:'+left_hand_position_y+'; left:'+left_hand_position_x+';"></div>');
	        // 	body.append('<div class="hand" id="hand-8" style="top:'+right_hand_position_y+'; left:'+right_hand_position_x+';"></div>');
	      		$('#hand-7').css('display', 'block');
		      	$('#hand-8').css('display', 'block');
	      		$('#hand-7').css('top', left_hand_position_y);
	      		$('#hand-7').css('left', left_hand_position_x);
	      		$('#hand-8').css('top', right_hand_position_y);
	      		$('#hand-8').css('left', right_hand_position_x);
	      	}
	      	else if(body.bodyIndex == 4){
	      		if(body.leftHandState == 4){
	        		$('#hand-9').css('background-image', 'url(images/hand-left-lasso.png');
	        	}else if(body.leftHandState == 3){
	        		$('#hand-9').css('background-image', 'url(images/hand-left-closed.png');
	        	}else{
	        		$('#hand-9').css('background-image', 'url(images/hand-left-open.png');
	        	}

	        	if(body.rightHandState == 4){
	        		$('#hand-10').css('background-image', 'url(images/hand-right-lasso.png');
	        	}else if(body.rightHandState == 3){
	        		$('#hand-10').css('background-image', 'url(images/hand-right-closed.png');
	        	}else{
	        		$('#hand-10').css('background-image', 'url(images/hand-right-open.png');
	        	}
	      		// var body = $('body');
	      		// body.append('<div class="hand" id="hand-9" style="top:'+left_hand_position_y+'; left:'+left_hand_position_x+';"></div>');
	        // 	body.append('<div class="hand" id="hand-10" style="top:'+right_hand_position_y+'; left:'+right_hand_position_x+';"></div>');
	      		$('#hand-9').css('display', 'block');
		      	$('#hand-10').css('display', 'block');
	      		$('#hand-9').css('top', left_hand_position_y);
	      		$('#hand-9').css('left', left_hand_position_x);
	      		$('#hand-10').css('top', right_hand_position_y);
	      		$('#hand-10').css('left', right_hand_position_x);
	      	}
	      	else if(body.bodyIndex == 5){
	      		if(body.leftHandState == 4){
	        		$('#hand-11').css('background-image', 'url(images/hand-left-lasso.png');
	        	}else if(body.leftHandState == 3){
	        		$('#hand-11').css('background-image', 'url(images/hand-left-closed.png');
	        	}else{
	        		$('#hand-11').css('background-image', 'url(images/hand-left-open.png');
	        	}

	        	if(body.rightHandState == 4){
	        		$('#hand-12').css('background-image', 'url(images/hand-right-lasso.png');
	        	}else if(body.rightHandState == 3){
	        		$('#hand-12').css('background-image', 'url(images/hand-right-closed.png');
	        	}else{
	        		$('#hand-12').css('background-image', 'url(images/hand-right-open.png');
	        	}
	      		// var body = $('body');
	      		// body.append('<div class="hand" id="hand-11" style="top:'+left_hand_position_y+'; left:'+left_hand_position_x+';"></div>');
	        // 	body.append('<div class="hand" id="hand-12" style="top:'+right_hand_position_y+'; left:'+right_hand_position_x+';"></div>');
	      		$('#hand-11').css('display', 'block');
		      	$('#hand-12').css('display', 'block');
	      		$('#hand-11').css('top', left_hand_position_y);
	      		$('#hand-11').css('left', left_hand_position_x);
	      		$('#hand-12').css('top', right_hand_position_y);
	      		$('#hand-12').css('left', right_hand_position_x);
	      	}
        }
        else{
        	if(body.bodyIndex == 0){
        		// $('#hand-1').remove();
        		// $('#hand-2').remove();
		      	$('#hand-1').css('display', 'none');
		      	$('#hand-2').css('display', 'none');
		      	$('#body-1-left-hand').text('Body 1 - left hand: [x: -] [y: -]');
		      	$('#body-1-right-hand').text('Body 1 - right hand: [x: -] [y: -]');
	        }
	      	else if(body.bodyIndex == 1){
	      		// $('#hand-3').remove();
        	// 	$('#hand-4').remove();
		      	$('#hand-3').css('display', 'none');
		      	$('#hand-4').css('display', 'none');
		      	$('#body-2-left-hand').text('Body 2 - left hand: [x: -] [y: -]');
		      	$('#body-2-right-hand').text('Body 2 - right hand: [x: -] [y: -]');
	      	}
	      	else if(body.bodyIndex == 2){
	      		// $('#hand-5').remove();
        	// 	$('#hand-6').remove();
	      		$('#hand-5').css('display', 'none');
		      	$('#hand-6').css('display', 'none');
		      	$('#body-3-left-hand').text('Body 3 - left hand: [x: -] [y: -]');
		      	$('#body-3-right-hand').text('Body 3 - right hand: [x: -] [y: -]');
	      	}
	      	else if(body.bodyIndex == 3){
	      		$('#hand-7').css('display', 'none');
		      	$('#hand-8').css('display', 'none');
		      	// $('#hand-7').remove();
        	// 	$('#hand-8').remove();
		      	$('#body-4-left-hand').text('Body 4 - left hand: [x: -] [y: -]');
		      	$('#body-4-right-hand').text('Body 4 - right hand: [x: -] [y: -]');
	      	}
	      	else if(body.bodyIndex == 4){
	      		$('#hand-9').css('display', 'none');
		      	$('#hand-10').css('display', 'none');
		      	// $('#hand-9').remove();
        	// 	$('#hand-10').remove();
		      	$('#body-5-left-hand').text('Body 5 - left hand: [x: -] [y: -]');
		      	$('#body-5-right-hand').text('Body 5 - right hand: [x: -] [y: -]');
	      	}
	      	else if(body.bodyIndex == 5){
	      		$('#hand-11').css('display', 'none');
		      	$('#hand-12').css('display', 'none');
		      	// $('#hand-11').remove();
        	// 	$('#hand-12').remove();
		      	$('#body-6-left-hand').text('Body 6 - left hand: [x: -] [y: -]');
		      	$('#body-6-right-hand').text('Body 6 - right hand: [x: -] [y: -]');
	      	}
        }
    });
}
